﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FMXLeanDog.Algorithm;
using FMXLeanDog.Algorithm.RKIBF;
using NUnit.Framework;

namespace FMXLeanDog.Tests
{
    public class AlgorithmTests
    {
        [Test]
		public void RKIBellmanFordTest()
		{
			var graph = new EdgeWeightedDirectedGraph(37);
			
			//start
			graph.AddEdge(new DirectedEdge(0, 1, 1));
			graph.AddEdge(new DirectedEdge(0, 2, 1));
			graph.AddEdge(new DirectedEdge(0, 3, 1));
			graph.AddEdge(new DirectedEdge(0, 4, 1));
			graph.AddEdge(new DirectedEdge(0, 5, 1));

			//column 1
			graph.AddEdge(new DirectedEdge(1, 6, 3));
			graph.AddEdge(new DirectedEdge(1, 7, 3));
			graph.AddEdge(new DirectedEdge(1, 10, 3));

			graph.AddEdge(new DirectedEdge(2, 6, 6));
			graph.AddEdge(new DirectedEdge(2, 7, 6));
			graph.AddEdge(new DirectedEdge(2, 8, 6));

			graph.AddEdge(new DirectedEdge(3, 7, 5));
			graph.AddEdge(new DirectedEdge(3, 8, 5));
			graph.AddEdge(new DirectedEdge(3, 9, 5));

			graph.AddEdge(new DirectedEdge(4, 8, 8));
			graph.AddEdge(new DirectedEdge(4, 9, 8));
			graph.AddEdge(new DirectedEdge(4, 10, 8));

			graph.AddEdge(new DirectedEdge(5, 9, 3));
			graph.AddEdge(new DirectedEdge(5, 10, 3));
			graph.AddEdge(new DirectedEdge(5, 6, 3));

			//column 2
			graph.AddEdge(new DirectedEdge(6, 11, 4));
			graph.AddEdge(new DirectedEdge(6, 12, 4));
			graph.AddEdge(new DirectedEdge(6, 15, 4));

			graph.AddEdge(new DirectedEdge(7, 11, -1));
			graph.AddEdge(new DirectedEdge(7, 12, -1));
			graph.AddEdge(new DirectedEdge(7, 13, -1));

			graph.AddEdge(new DirectedEdge(8, 12, 9));
			graph.AddEdge(new DirectedEdge(8, 13, 9));
			graph.AddEdge(new DirectedEdge(8, 14, 9));

			graph.AddEdge(new DirectedEdge(9, 13, 4));
			graph.AddEdge(new DirectedEdge(9, 14, 4));
			graph.AddEdge(new DirectedEdge(9, 15, 4));

			graph.AddEdge(new DirectedEdge(10, 14, 7));
			graph.AddEdge(new DirectedEdge(10, 15, 7));
			graph.AddEdge(new DirectedEdge(10, 11, 7));

			//column 3
			graph.AddEdge(new DirectedEdge(11, 16, 1));
			graph.AddEdge(new DirectedEdge(11, 17, 1));
			graph.AddEdge(new DirectedEdge(11, 20, 1));

			graph.AddEdge(new DirectedEdge(12, 16, 8));
			graph.AddEdge(new DirectedEdge(12, 17, 8));
			graph.AddEdge(new DirectedEdge(12, 18, 8));

			graph.AddEdge(new DirectedEdge(13, 17, 3));
			graph.AddEdge(new DirectedEdge(13, 18, 3));
			graph.AddEdge(new DirectedEdge(13, 19, 3));

			graph.AddEdge(new DirectedEdge(14, 18, 1));
			graph.AddEdge(new DirectedEdge(14, 19, 1));
			graph.AddEdge(new DirectedEdge(14, 20, 1));

			graph.AddEdge(new DirectedEdge(15, 19, 2));
			graph.AddEdge(new DirectedEdge(15, 20, 2));
			graph.AddEdge(new DirectedEdge(15, 16, 2));

			//column 4
			graph.AddEdge(new DirectedEdge(16, 21, 2));
			graph.AddEdge(new DirectedEdge(16, 22, 2));
			graph.AddEdge(new DirectedEdge(16, 25, 2));

			graph.AddEdge(new DirectedEdge(17, 21, 2));
			graph.AddEdge(new DirectedEdge(17, 22, 2));
			graph.AddEdge(new DirectedEdge(17, 23, 2));

			graph.AddEdge(new DirectedEdge(18, 22, 9));
			graph.AddEdge(new DirectedEdge(18, 23, 9));
			graph.AddEdge(new DirectedEdge(18, 24, 9));

			graph.AddEdge(new DirectedEdge(19, 23, 3));
			graph.AddEdge(new DirectedEdge(19, 24, 3));
			graph.AddEdge(new DirectedEdge(19, 25, 3));

			graph.AddEdge(new DirectedEdge(20, 24, 8));
			graph.AddEdge(new DirectedEdge(20, 25, 8));
			graph.AddEdge(new DirectedEdge(20, 21, 8));

			//column 5
			graph.AddEdge(new DirectedEdge(21, 26, 8));
			graph.AddEdge(new DirectedEdge(21, 27, 8));
			graph.AddEdge(new DirectedEdge(21, 30, 8));

			graph.AddEdge(new DirectedEdge(22, 26, 7));
			graph.AddEdge(new DirectedEdge(22, 27, 7));
			graph.AddEdge(new DirectedEdge(22, 28, 7));

			graph.AddEdge(new DirectedEdge(23, 27, 9));
			graph.AddEdge(new DirectedEdge(23, 28, 9));
			graph.AddEdge(new DirectedEdge(23, 29, 9));

			graph.AddEdge(new DirectedEdge(24, 28, 2));
			graph.AddEdge(new DirectedEdge(24, 29, 2));
			graph.AddEdge(new DirectedEdge(24, 30, 2));

			graph.AddEdge(new DirectedEdge(25, 29, 6));
			graph.AddEdge(new DirectedEdge(25, 30, 6));
			graph.AddEdge(new DirectedEdge(25, 26, 6));

			//column 6
			graph.AddEdge(new DirectedEdge(26, 31, 6));
			graph.AddEdge(new DirectedEdge(26, 32, 6));
			graph.AddEdge(new DirectedEdge(26, 35, 6));

			graph.AddEdge(new DirectedEdge(27, 31, 4));
			graph.AddEdge(new DirectedEdge(27, 32, 4));
			graph.AddEdge(new DirectedEdge(27, 33, 4));

			graph.AddEdge(new DirectedEdge(28, 32, 5));
			graph.AddEdge(new DirectedEdge(28, 33, 5));
			graph.AddEdge(new DirectedEdge(28, 34, 5));

			graph.AddEdge(new DirectedEdge(29, 33, 6));
			graph.AddEdge(new DirectedEdge(29, 34, 6));
			graph.AddEdge(new DirectedEdge(29, 35, 6));

			graph.AddEdge(new DirectedEdge(30, 34, 4));
			graph.AddEdge(new DirectedEdge(30, 35, 4));
			graph.AddEdge(new DirectedEdge(30, 31, 4));

			//end
			graph.AddEdge(new DirectedEdge(31, 36, 1));
			graph.AddEdge(new DirectedEdge(32, 36, 1));
			graph.AddEdge(new DirectedEdge(33, 36, 1));
			graph.AddEdge(new DirectedEdge(34, 36, 1));
			graph.AddEdge(new DirectedEdge(35, 36, 1));

			var bellmanFord = new BellmanFord(graph, 0);
			var pathTo35 = bellmanFord.PathTo(36);
			var distTo35 = bellmanFord.GetDistTo(36);
			//var result = bf.GetShortestPath(0, out edgesInPath);
			//Debug.Print();
		}
    }
}
