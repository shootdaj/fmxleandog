﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FMXLeanDog
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
			routes.RouteExistingFiles = true;

			routes.MapRoute(
				name: "staticFileRoute",
				url: "EditableGrid/{file}",
				defaults: new {controller = "Home", action = "SomeAction"}
				);

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "PathOfLeastResistance", action = "Index", id = UrlParameter.Optional }
			);

			//routes.MapPageRoute("HtmlRoute", "EditableGrid", "~/Views/EditableGrid");
		}
	}
}
