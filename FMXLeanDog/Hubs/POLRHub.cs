﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using FMXLeanDog.Algorithm;
using FMXLeanDog.Algorithm.RKIBF;
using FMXLeanDog.Models;
using IronPython.Modules;
using Microsoft.AspNet.SignalR;
using WebGrease.Css.Extensions;

namespace FMXLeanDog.Hubs
{
	public class PolrHub : Hub
	{
		private const int MaxResistance = 50;

		public void Calculate(GridElement[][] grid)
		{
			//clear path
			grid.ForEach(row => row.ForEach(element => element.InShortestPath = false));

			//do calculation and set the InShortestPath property to true 
			//for the elements that are in the shortest path
			bool overflow = false;
			var modifiedGrid = FindPOLR(grid, out overflow);

			//refresh grid at client
			Clients.Caller.refreshGrid(modifiedGrid, overflow);
		}

		private GridElement[][] FindPOLR(GridElement[][] inputGrid, out bool overflow)
		{
			//verify grid structure
			VerifyGrid(inputGrid);

			//var graphWidth = grid.Length + 1;
			var graphHeight = inputGrid.Length;

			//prepare graph from grid
			var graph = PrepareGraph(inputGrid);

			//process graph
			var bellmanFord = new BellmanFord(graph, 0);

			//get shortest path
			var polr = bellmanFord.PathTo(graph.V() - 1);
			//var polrDistance = bellmanFord.DistTo(graph.V() - 1);

			//assert grid's InShortestPath for the applicable elements (remove first and last because they are dummies)
			var totalResistance = 0;
			var polrEnumerated = polr as IList<DirectedEdge> ?? polr.ToList();
			polrEnumerated.Except(new List<DirectedEdge>() { polrEnumerated.First(), polrEnumerated.Last()}).ForEach(edge =>
			{
				var column = (int)( edge.From() - 1)/(int) graphHeight + 1;
				var row = edge.From()%graphHeight == 0 ? graphHeight : edge.From()%graphHeight;
				totalResistance += inputGrid[row - 1][column - 1].Weight;
				if (totalResistance <= MaxResistance)
					inputGrid[row - 1][column - 1].InShortestPath = true;
			});

			overflow = totalResistance > MaxResistance;

			//return the modified grid
			return inputGrid;
		}

		private EdgeWeightedDirectedGraph PrepareGraph(GridElement[][] grid)
		{
			var graphHeight = grid.Length;
			var graphWidth = grid[0].Length + 1;

			var graph = new EdgeWeightedDirectedGraph(graphWidth * graphHeight + 2);		//V = n x m + 2 vertices for starting and ending points

			//connect the starting point to the grid's first column
			for (int i = 1; i <= graphHeight; i++)
			{
				graph.AddEdge(new DirectedEdge(0, i, 1));
			}

			//connect each vertex to its frontwards neighbors
			for (int i = 1; i <= graphWidth - 1; i++)
			{
				for (int j = 1; j <= graphHeight; j++)
				{
					if (j%graphHeight == 1) //top row
					{
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j, grid[j - 1][i - 1].Weight));
							//connect to horizontal neighbor
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j + 1, grid[j - 1][i - 1].Weight));
							//connect to lower diagonal
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + graphHeight, grid[j - 1][i - 1].Weight));
							//connect to upper diagonal (wrapped)
					}
					else if (j%graphHeight == 0) //bottom row
					{
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j, grid[j - 1][i - 1].Weight));
							//connect to horizontal neighbor
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + 1, grid[j - 1][i - 1].Weight));
							//connect to lower diagonal (wrapped)
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j - 1, grid[j - 1][i - 1].Weight));
							//connect to upper diagonal
					}
					else
					{
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j, grid[j - 1][i - 1].Weight));
							//connect to horizontal neighbor
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j + 1, grid[j - 1][i - 1].Weight));
							//connect to lower diagonal
						graph.AddEdge(new DirectedEdge(((i - 1)*graphHeight) + j, i*graphHeight + j - 1, grid[j - 1][i - 1].Weight));
							//connect to upper diagonal
					}
				}
			}

			//connect the grid's last column to the ending point
			for (int i = graph.V() - 2; i >= graph.V() - 1 - graphHeight; i--)
			{
				graph.AddEdge(new DirectedEdge(i, graph.V() - 1, 1));
			}

			return graph;
		}

		public bool IsDivisble(int x, int n)
		{
			return (x % n) == 0;
		}

		private void VerifyGrid(GridElement[][] grid)
		{
			if (grid.Length == 0)
				throw new Exception("There are no elements in this grid. Grid is invalid.");
			if (grid[0].Length == 0)
				throw new Exception("At least one row in this grid has no columns. Grid is invalid.");

			var numColumns = grid[0].Length;
			if (grid.Any(row => row.Length != numColumns))
				throw new Exception("The provided staggered array is not in the form of a grid. At least two of the rows have different number of columns.");
		}
	}

}