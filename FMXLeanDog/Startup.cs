﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(FMXLeanDog.Startup))]

namespace FMXLeanDog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

			// Any connection or hub wire up and configuration should go here
			app.MapSignalR();
		}
    }
}
