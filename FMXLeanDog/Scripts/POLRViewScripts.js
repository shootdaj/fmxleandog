﻿
var polr;
var editableGrid;

window.onload = function () {

	$('#btnCalculate').click(function () {
		calculate();
	});

	//initialize grid
	editableGrid = new EditableGrid("POLRGrid");
	editableGrid.tableLoaded = function () { this.renderGrid("tablecontent", "polrGrid"); };

	//load from json file
	//editableGrid.loadJSON("Content/inputData.json");

	//load from text file
	//MapTextFileToGrid("Content/inputTextData.txt");

	//OR create large dataset on the fly
	//createRandomDatasetAndMapToGrid(3, 3, 3, true);
}

$(function appStart() {
	// Reference the auto-generated proxy for the hub.
	polr = $.connection.polrHub;
	// Create a function that the hub can call back to refresh grid.
	polr.client.refreshGrid = function (grid, overflow) {
		var metadata = [];
		//create metadata - the header row - not really needed, just there for the requirements of editablegrids
		createMetadata(metadata, grid[0].length);

		var data = [];
		for (var i = 0; i < grid.length; i++) {
			var row = grid[i];
			var values = {};
			for (var j = 0; j < row.length; j++) {
				values[j] = row[j].Weight;
			}
			data.push({ id: i + 1, values: values });
		}

		editableGrid.load({ "metadata": metadata, "data": data });
		editableGrid.renderGrid("tablecontent", "polrGrid");

		showResults(grid, overflow);
	};

	// Start the connection.
	$.connection.hub.start().done(function () {
		mapButtons();
	});
});

function mapButtons() {
	$("#btnCalculateFromTextInput").click(function () {
		calculateFromTextInput($("#txtGrid").val());
	}).children().click(function (e) {
		return false;
	});

	$("#btnCalculateFromInputFileAtURL").click(function () {
		calculateFromURL($("#txtURL").val(), $('input:radio[name=optFileFormat]:checked').val());
	}).children().click(function (e) {
		e.stopPropagation();
	});


	$("#btnCalculateFromRandomGrid").click(function () {
		createRandomDatasetAndMapToGrid(
			parseInt($("#txtWidth").val(), 10),
			parseInt($("#txtHeight").val(), 10),
			parseInt($("#txtMax").val(), 10),
			$("#chkNegative").prop("checked"));
		calculate();
	}).children().click(function (e) {
		e.stopPropagation();
	});

}

function calculateFromURL(url, format) {
	if (format === "json") {
		calculateFromJsonURL(url);
	}
	else if (format === "text") {
		$.get(url, function (data) {
			calculateFromTextInput(data);
		}, format);
	}
}

function calculateFromJsonURL(url) {
	editableGrid.loadJSON(url, function() { calculate(); });
}

function calculateFromTextInput(input) {
	if ($.trim(input) === '') {
		return false;
	}

	var metadata = [];
	var data = [];
	
	//convert input text into data
	var width = convertTextIntoData(input, data);
	
	//create metadata
	createMetadata(metadata, width);

	//gridify
	loadIntoGrid(metadata, data);

	//calculate
	calculate();
}

function createRandomDatasetAndMapToGrid(width, height, maxWeight, negativeWeights) {
	//var width = 50;
	//var height = 10;

	var metadata = [];
	//create metadata - the header row - not really needed, just there for the requirements of editablegrids
	for (var i = 0; i < width; i++) {
		metadata.push({ name: i, label: i, datatype: "integer", editable: true });
	}

	var data = [];
	for (var i = 0; i < height; i++) {
		var values = {};
		for (var j = 0; j < width; j++) {
			values[j] = (negativeWeights && Math.random() < 0.5) ? (-1 * Math.floor((Math.random() * maxWeight) + 1)) : Math.floor((Math.random() * maxWeight) + 1);
		}
		data.push({ id: i + 1, values: values });
	}

	editableGrid.load({ "metadata": metadata, "data": data });
	editableGrid.renderGrid("tablecontent", "polrGrid");
}

function convertTextIntoData(input, data) {
	var lines = input.split("\n");
	var width = 0;
	for (var i = 0; i < lines.length; i++) {
		var weights = $.trim(lines[i]).split(/\s+/);
		var values = {};
		width = weights.length;
		for (var j = 0; j < weights.length; j++) {
			if (!isNaN(weights[j]) && weights[j] !== "") {
				values[j] = parseInt(weights[j]);
			}
		}
		data.push({ id: i + 1, values: values });
	}

	return width;
}

function createMetadata(metadata, width) {
	for (var i = 0; i < width; i++) {
		metadata.push({ name: i, label: i, datatype: "integer", editable: true });
	}
}

function calculate() {

	//create array from table
	var gridArray = [];
	createArrayFromTable(gridArray);

	//call the calculate method on the hub.
	polr.server.calculate(gridArray);
}

function createArrayFromTable(gridArray) {
	$("table.polrGrid tr").each(function (row, tr) {
		var rowArray = [];
		var td = $(this).find("td");
		if (td.length > 0) {
			td.each(function () {
				rowArray.push({ "Weight": $(this).text(), "InShortestPath": "false" });
			});
			gridArray.push(rowArray);
		}
	});
}

function showResults(grid, overflow) {

	//clear output labels
	$("#success").text("");
	$("#distance").text("");
	$("#path").text("");

	//set overflow text
	if (overflow)
		$("#success").text("No");
	else
		$("#success").text("Yes");

	var totalDistance = 0;

	//create a list of <td> indices to highlight and gather total distance and path
	var rowTdsToHighlight = [];
	for (var i = 0; i < grid.length; i++) {
		var row = grid[i];
		rowTdsToHighlight[i] = [];
		for (var j = 0; j < row.length; j++) {
			if (grid[i][j].InShortestPath === true) {
				rowTdsToHighlight[i].push(j);
				totalDistance += grid[i][j].Weight;
			}
		}
	}

	//set total distance
	$("#distance").text(totalDistance.toString());

	//highlight items in grid and show them in the path label
	for (var i = 0; i < rowTdsToHighlight.length; i++) {
		for (var j = 0; j < rowTdsToHighlight[i].length; j++) {
			if (!overflow)
				$("table.polrGrid tr:nth-child(" + (i + 1) + ") td:nth-child(" + (rowTdsToHighlight[i][j] + 1) + ")").css("background-color", "rgb(103, 226, 98)");
			else
				$("table.polrGrid tr:nth-child(" + (i + 1) + ") td:nth-child(" + (rowTdsToHighlight[i][j] + 1) + ")").css("background-color", "rgb(170, 0, 12)");
		}
	}

	//get the result in text form
	for (var j = 0; j < $("table.polrGrid tr:nth-child(1)").children("td").length; j++) {
		for (var i = 0; i < $("table.polrGrid tr").length - 1; i++) {
			if (grid[i][j].InShortestPath === true) {
				$("#path").append((i + 1).toString() + " ");
			}
		}
	}
}

function createRandomDatasetAndMapToGrid(width, height, maxWeight, negativeWeights) {
	//var width = 50;
	//var height = 10;

	var metadata = [];
	
	createMetadata(metadata, width);

	var data = [];
	for (var i = 0; i < height; i++) {
		var values = {};
		for (var j = 0; j < width; j++) {
			values[j] = (negativeWeights && Math.random() < 0.5) ? (-1 * Math.floor((Math.random() * maxWeight) + 1)) : Math.floor((Math.random() * maxWeight) + 1);
		}
		data.push({ id: i + 1, values: values });
	}

	editableGrid.load({ "metadata": metadata, "data": data });
	editableGrid.renderGrid("tablecontent", "polrGrid");
}

function loadIntoGrid(metadata, data) {
	editableGrid.load({ "metadata": metadata, "data": data });
	editableGrid.renderGrid("tablecontent", "polrGrid");
}