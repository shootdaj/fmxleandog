﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FMXLeanDog.Models
{
	public class GridElement
	{
		public int Weight { get; set; }
		public bool InShortestPath { get; set; }	
	}
}