﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;

namespace FMXLeanDog.IronPython
{
	public class JohnsonsAlgorithmProxy
	{
		public ScriptEngine PythonEngine { get; set; }
		public dynamic Scope { get; set; }
		
		public JohnsonsAlgorithmProxy()
		{
			PythonEngine = Python.CreateEngine();
			Scope = PythonEngine.CreateScope();
		}
	}
}