using System;
using System.Collections.Generic;
using System.Linq;
using FMXLeanDog.Algorithm.RKIBF;

namespace FMXLeanDog.Algorithm
{
	/// <summary>
	/// An implementation of Bellman-Ford shortest path algorithm.
	/// The inputs are an edge weighted directed graph and an individual vertex in the graph.
	/// The output will be the shortest paths from the vertex to all other vertices it is connected to.
	/// </summary>
	public class BellmanFord
	{
		/// <summary>
		/// An array to keep track of the distance from our starting vertex to all connected vertices. 
		/// </summary>
		public double[] DistTo { get; private set; }

		/// <summary>
		/// an array of edges in our shortest paths
		/// it is interesting to not for all vertices in the shortest paths
		/// there is only one edge to each vertex which is why we can use 
		/// a simple DirectedEdge array
		/// </summary>
		public DirectedEdge[] EdgeTo { get; private set; }


		/// <summary>
		/// Helps keeps track of which vertices are in our shortest paths
		/// </summary>
		public bool[] OnQueue { get; private set; }

		/// <summary>
		/// Helps keeps track of which vertices are in our shortest paths
		/// </summary>
		public Queue<int> Queue { get; private set; }

		public int Cost { get; private set; }

		/// <summary>
		/// Keeps track of edges that end up in a negative-edge cycle
		/// </summary>
		private IEnumerable<DirectedEdge> _cycle;

		/// <summary>
		/// The inputs are an edge weighted directed graph and a starting vertex
		/// </summary>
		public BellmanFord(EdgeWeightedDirectedGraph graph, int startingVertex)
		{
			//initialize our arrays and set the default values
			//for weights to positive infinity
			DistTo = new double[graph.V()];
			EdgeTo = new DirectedEdge[graph.V()];
			OnQueue = new bool[graph.V()];
			for (int v = 0; v < graph.V(); v++)
				DistTo[v] = Double.PositiveInfinity;
			DistTo[startingVertex] = 0.0;

			Queue = new Queue<int>();
			Queue.Enqueue(startingVertex);
			OnQueue[startingVertex] = true;
			while (!(Queue.Count() <= 0) && !HasNegativeCycle())
			{
				//get a vertex from the queue and remove it from the queue
				int v = Queue.Dequeue();
				OnQueue[v] = false;
				Relax(graph, v);
			}
		}

		/// <summary>
		/// Relaxes all the edges with the given vertex
		/// </summary>
		/// <param name="G"></param>
		/// <param name="v"></param>
		private void Relax(EdgeWeightedDirectedGraph G, int v)
		{

			//for each edge connected to the vertex v:
			//   -get a reference to the target vertex (w)
			//   -is the distance to w through v shorter than any other path?
			//	 -update the DistTo[] array with the new path weight
			//	 -add the new edge to the EdgeTo array
			//	 -add w to the end of Queue and mark that it is in the queue
			foreach (DirectedEdge e in G.Adj(v))
			{
				int w = e.To();
				if (DistTo[w] > DistTo[v] + e.Weight())
				{
					DistTo[w] = DistTo[v] + e.Weight();
					EdgeTo[w] = e;
					if (!OnQueue[w])
					{
						Queue.Enqueue(w);
						OnQueue[w] = true;
					}
				}

				//each time we look at an edge increment the _cost variable
				//when it is equal to a multiple of the number of vertices check
				//for an negative cycle
				if (Cost++ % G.V() == 0)
					FindNegativeCycle();
			}
		}

		//Indicates whether we have a negative cycle
		public bool HasNegativeCycle()
		{
			return _cycle != null;
		}

		//A iterable representation of the negative cycle
		public IEnumerable<DirectedEdge> NegativeCycle()
		{
			return _cycle;
		}

		/// <summary>
		/// Use the EdgeWeightedDirectedCycle data structure to see if there is a negative cycle in our graph
		/// </summary>
		private void FindNegativeCycle()
		{
			//we don't want to look at the entire graph, just the 
			//graph we've examined so far
			//use the edges in the _edgeTo array to create a new 
			//edge weighted directed graph and pass that to EdgeWeightedDirectedCycle 
			int V = EdgeTo.Length;
			EdgeWeightedDirectedGraph spt;
			spt = new EdgeWeightedDirectedGraph(V);
			for (int v = 0; v < V; v++)
			{
				if (EdgeTo[v] != null)
					spt.AddEdge(EdgeTo[v]);
			}

			EdgeWeightedDirectedCycle finder = new EdgeWeightedDirectedCycle(spt);
			//_cycle will contain our negative edges
			_cycle = finder.Cycle();
		}

		/// <summary>
		/// Does the graph contain a path from the source vertex to vertex v?
		/// </summary>
		public bool HasPathTo(int v)
		{
			return DistTo[v] < Double.PositiveInfinity;
		}

		/// <summary>
		/// Return the shortest distance to any connected vertex v
		/// </summary>
		public double GetDistTo(int v)
		{
			return DistTo[v];
		}

		/// <summary>
		/// Get the path (the series of edges/vertices) to get from the source vertex to a given connected vertex v as an iterable list.
		/// </summary>
		/// <param name="v">Vertex to get the path to.</param>
		public IEnumerable<DirectedEdge> PathTo(int v)
		{
			if (!HasPathTo(v)) return null;
			Stack<DirectedEdge> path = new Stack<DirectedEdge>();
			for (DirectedEdge e = EdgeTo[v]; e != null; e = EdgeTo[e.From()])
			{
				path.Push(e);
			}
			return path;
		}
	}
}