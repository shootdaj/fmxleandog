﻿using System.Collections.Generic;

namespace FMXLeanDog.Algorithm
{
	/// <summary>
	/// Implementation of an edge weighted directed graph
	/// </summary>
	public class EdgeWeightedDirectedGraph
	{
		private readonly int _v; //The number of vertices
		private int _e;//The number of edges
		private LinkedList<DirectedEdge>[] _adj;//A linked list representation of the adjacency lists

		/// <summary>
		/// Create an edge weighted directed graph with V vertices
		/// </summary>
		/// <param name="V">Number of vertices</param>
		public EdgeWeightedDirectedGraph(int V)
		{
			this._v = V;
			this._e = 0;

			//create v linked lists, one for each vertex, which keeps track
			//of the edge from v to other vertices v 
			_adj = new LinkedList<DirectedEdge>[V];
			for (int v = 0; v < _v; v++)
			{
				_adj[v] = new LinkedList<DirectedEdge>();
			}
		}

		/// <summary>
		/// Return the number of vertices
		/// </summary>
		public int V()
		{
			return _v;
		}

		/// <summary>
		/// Return the number of edges
		/// </summary>
		public int E()
		{
			return _e;
		}

		/// <summary>
		/// Add an edge at the start of the linked list and increase the edge count.
		/// </summary>
		/// <param name="e"></param>
		public void AddEdge(DirectedEdge e)
		{
			_adj[e.From()].AddFirst(e);
			_e++;
		}

		/// <summary>
		/// Allows iteration through the vertices linked lists of a single vertex.
		/// </summary>
		public IEnumerable<DirectedEdge> Adj(int v)
		{
			return _adj[v];
		}

		/// <summary>
		/// Allows iteration through the edges
		/// </summary>
		public IEnumerable<DirectedEdge> Edges()
		{
			LinkedList<DirectedEdge> linkedlist = new LinkedList<DirectedEdge>();
			for (int v = 0; v < _v; v++)
			{
				foreach (DirectedEdge e in _adj[v])
					linkedlist.AddFirst(e);
			}
			return linkedlist;
		}
	}

}