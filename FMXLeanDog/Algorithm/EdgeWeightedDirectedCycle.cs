using System.Collections.Generic;

namespace FMXLeanDog.Algorithm.RKIBF
{
	/// <summary>
	/// This class will allow us to find, if any, a negative cycle
	/// for a given edge weighted directed graph.
	/// </summary>
	public class EdgeWeightedDirectedCycle
	{
		/// <summary>
		/// Helps us keep track of the vertices we've examined
		/// </summary>
		public bool[] Marked { get; private set; }

		/// <summary>
		/// As we examine edges that may be in a negative cycle, _edgeTo helps us 
		/// keep track of the target vertices in those edges.
		/// </summary>
		public DirectedEdge[] EdgeTo { get; private set; }

		/// <summary>
		/// Helps us keep track of vertices that might be in a negative cycle.  If we 
		///want to examine the vertex we mark it as true and once we've examined it we mark it as false
		/// </summary>
		public bool[] OnStack { get; private set; }

		/// <summary>
		/// Initialize with a given graph G
		/// </summary>
		public EdgeWeightedDirectedCycle(EdgeWeightedDirectedGraph graph)
		{
			//intialize our arrays to the size of the number of vertices in the graph
			Marked = new bool[graph.V()];
			OnStack = new bool[graph.V()];
			EdgeTo = new DirectedEdge[graph.V()];
			
			for (int v = 0; v < graph.V(); v++)
				if (!Marked[v]) DFS(graph, v);
		}

		/// <summary>
		/// Determine whether there is a negative cycle from vertex v, add the edges of the negative cycle to _cycle 
		/// </summary>
		private void DFS(EdgeWeightedDirectedGraph G, int v)
		{
			//set the indices for this vertex in the _onStack and _marked arrrays as true
			OnStack[v] = true;
			Marked[v] = true;
			DirectedEdge ecopy;
			
			//examine each edge connected to v
			foreach (DirectedEdge e in G.Adj(v))
			{
				//create a copy of the current edge we are looking at
				ecopy = e;
				//get the target vertex
				int w = ecopy.To();

				//we've already found a negative cycle, so go ahead and exit
				if (_cycle != null)
					return;
				
				//we haven't looked at the target vertex(w) yet, add it to the _edgeTo array and call
				//DFS for this edge
				else if (!Marked[w])
				{
					EdgeTo[w] = ecopy;
					DFS(G, w);
				}
				
				//if a vertex is marked and is on the stack then we have a negative cycle
				//add the edge to _cycle
				else if (OnStack[w])
				{
					_cycle = new Stack<DirectedEdge>();
					while (ecopy.From() != w)
					{
						_cycle.Push(ecopy);
						ecopy = EdgeTo[ecopy.From()];
					}
					_cycle.Push(ecopy);
				}
			}
			OnStack[v] = false;
		}

		/// <summary>
		/// Indicates whether we've found a negative cycle
		/// </summary>
		public bool HasCycle() { return _cycle != null; }

		/// <summary>
		/// Returns the negative cycle and an iterable list of edges.
		/// </summary>
		public IEnumerable<DirectedEdge> Cycle() { return _cycle; }

		/// <summary>
		/// If we find an negative cycle add those edges to this stack.
		/// </summary>
		private Stack<DirectedEdge> _cycle;
	}
}