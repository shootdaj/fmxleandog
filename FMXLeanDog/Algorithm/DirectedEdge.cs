using System;

namespace FMXLeanDog.Algorithm
{
	/// <summary>
	///	A representation of an edge from a source vertex to a target vertex with a weight.
	/// </summary>
	public class DirectedEdge
	{
		/// <summary>
		/// The source vertex
		/// </summary>
		private readonly int _v;

		/// <summary>
		/// The target vertex
		/// </summary>
		private readonly int _w;

		/// <summary>
		/// The weight to go from _v to _w
		/// </summary>
		private readonly double _weight;

		/// <summary>
		/// Create a directed edge from v to w with the given weight.
		/// </summary>
		public DirectedEdge(int v, int w, double weight)
		{
			this._v = v;
			this._w = w;
			this._weight = weight;
		}

		/// <summary>
		/// Return the weight.
		/// </summary>
		public double Weight()
		{
			return _weight;
		}

		/// <summary>
		/// Return the source vertex.
		/// </summary>
		public int From()
		{
			return _v;
		}

		/// <summary>
		/// Return the target vertex.
		/// </summary>
		public int To()
		{
			return _w;
		}

		/// <summary>
		/// Return a string representation of the edge.
		/// </summary>
		public override string ToString()
		{
			return String.Format("{0:d}->{1:d} {2:f}", _v, _w, _weight);
		}
	}
}